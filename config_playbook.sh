#!/bin/bash
repository=$1
branch=$2
dst="/home/ubuntu/repositories/$repository"
cat <<EOF >> ansible/playbook.yaml
  - name: Checkout
    ansible.builtin.git:
      repo: "git@bitbucket.org:7chairs/$repository.git"
      dest: $dst
      accept_hostkey: yes
      key_file: /home/ubuntu/.ssh/maagalim_asg_allinone_demo
      version: $branch
EOF

'''
if [[ $repository == "public-site" ]];
then
cat <<EOF >> ansible/playbook.yaml
  - name: npm build & upload to s3
    shell: |
      cd /home/ubuntu/repositories/public-site
      sudo npm install
      sudo npm run build
      aws s3 sync public s3://circles-apanel-demo/
EOF
fi
'''

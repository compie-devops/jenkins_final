#!/bin/bash

# 01. Get token

access_token=$(curl -X POST -u "$1:$2" https://bitbucket.org/site/oauth2/access_token -d grant_type=refresh_token -d refresh_token="$3" | jq '.access_token')

# 02. Get repositories

echo "None" > all_repositories

curl --request GET \
  --url "https://api.bitbucket.org/2.0/repositories/7Chairs" \
  --header "Authorization: Bearer $access_token" \
  --header 'Accept: application/json' | jq --raw-output '.values[].links.clone[1].href' >> all_repositories


sed -i 's/.*7chairs\/\(.*\).git.*/\1/' all_repositories


# check
cat all_repositories


# 03. Get all branches for all repositories

mkdir -p branches

while read -r repo;
do

	echo -e "\nrepo:  $repo\n" ;

	curl --request GET \
  	  --url "https://api.bitbucket.org/2.0/repositories/7Chairs/$repo/refs/branches" \
  	  --header "Authorization: Bearer $access_token" \
  	  --header 'Accept: application/json' | jq '.' > data.json

	jq --raw-output '.values[].name' data.json > branches/$repo


	while [ $(jq 'has("next")' data.json) ]
	do
		current_qurey_url="$(jq --raw-output '.next' data.json)"
		curl --request GET \
	  	  --url "$current_qurey_url" \
	  	  --header "Authorization: Bearer $access_token" \
	  	  --header 'Accept: application/json' | jq '.' > data.json

		jq '.values[].name' data.json >> branches/$repo

done

done < all_repositories

# check
echo -e "all branches"; ls branches/

